## Build Instructions

Please make sure you installed [docker](https://docs.docker.com/install/). Clone the project and open a terminal in its root directory.

##### Windows

```powershell
docker run --rm -it -v "${PWD}:/docs" squidfunk/mkdocs-material build
```

##### Other

```bash
docker run --rm -it -v "$(pwd):/docs" squidfunk/mkdocs-material build
```

## Working on the documentation

Please make sure you are able to build the project. Open a terminal in the project root directory. We can use Docker to continuously update and refresh our website while making changes on the documentation using the following command.

##### Windows

```powershell
docker run --rm -it -v "${PWD}:/docs" -p 8000:8000 squidfunk/mkdocs-material
```

##### Other

```bash
docker run --rm -it -v "$(pwd)/docs" -p 8000:8000 squidfunk/mkdocs-material
```

This way you can open your browser on port `8000` to see your changes. The container itself will detect your changes in the documentation and rebuild the website automatically.

Before you begin writing on the documentation please make sure you know the basics of [mkdocs](https://www.mkdocs.org/user-guide/writing-your-docs/) and [mkdocs-material](https://squidfunk.github.io/mkdocs-material/). Additionally, it is beneficial to know the most common plugins used here. You can find a good summarization [here](https://squidfunk.github.io/mkdocs-material/extensions/pymdown/).
