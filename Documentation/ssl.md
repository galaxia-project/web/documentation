# Secure Your Data Using SSL

Galaxia has built-in [SSL](https://en.wikipedia.org/wiki/Transport_Layer_Security) support. This enables you to secure your data from malicious software that may track your actions using the wallet or payment gate service. Unfortunately, we are not able to provide you a one-click setup since this would give us a potential chance to cheat you. Since we are a privacy coin we take anonymity very seriously and want to serve you with a guide that makes setting up a secure connection as simple as possible.

## Prerequisite Installation

In order to secure your connection, we are required to set up a key that only you have access to. These keys can be generated using `SSL` clients. While there are a bunch of them out there we will focus on `OpenSSL`. The following scripts are copy-paste solutions to install `OpenSSL` from prebuilt installers. If you want to build `OpenSSL` yourself and verify it is not compromised you may check out their [official wiki](https://wiki.openssl.org/).

```PowerShell tab="Windows"
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
choco install -y openssl.light
```

```bash tab="apt-get"
sudo apt-get update
sudo apt-get install -y openssl
```

```bash tab="zypper"
sudo zypper refresh
sudo zypper --non-interactive install openssl
```

??? tip "PowerShell Administrator Console"
    You can open an administrator PowerShell console by right-clicking your windows start menu icon and click on `Windows PowerShell (Admin)`.

Once the installation process went threw you should be able to use the `openssl` command. You can verify everything setup correctly by executing `openssl version -v`, which should yield something like `OpenSSL 1.0.2g  1 Mar 2016` dependent on your installed version.

## Generating Private Keys

Now we are ready to generate our own secret keys without being dependent on anyone else doing this for us, who may want to harm us. Therefore we need to open a terminal where our Galaxia binaries are installed.

??? tip "Opening Terminals From Explorer"
    On Linux based systems you normally should be able to open a console in a directory by right-clicking on some empty space and select `Terminal`. On Windows systems this is also possible by holding <kbd>CTRL</kbd>+<kbd>SHIFT</kbd> and right-clicking on some empty space. You should see a new context menu entry named `Open PowerShell window here`.

Once we opened a terminal we can generate the necessary keys for the server. The server searches, by default, in the subdirectory `./ssl` for three files `dh.pem`, `cert.pem` and `key.pem`. The following script can generate these for you.

!!! danger "Modify The Password"
    If your private key is not locked using a password other applications may be able to read your private key and infiltrate your connection. Replace the `<changeme>` bracket with a secure password. If your password has less than eight signs the server will print a warning that your setup is insecure.

```PowerShell tab="Windows"
New-Item -ItemType Directory -Path .\ssl -Force
Push-Location .\ssl
openssl dhparam -out dh.pem 2048
openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 10000 -out cert.pem -passout pass:<changeme>
Pop-Location
```

```bash tab="Linux"
mkdir -p ./ssl
cd ./ssl
openssl dhparam -out dh.pem 2048
openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 10000 -out cert.pem -passout pass:<changeme>
cd ..
```

During the process you will need to fill some data, you do not need to bother too much about them just pick something you feel comfortable with and you can recognize if you see it again.

## Verify Your Key

Now we need to configure all Galaxia applications to accept servers using our generated key. By default Galaxia applications will search for the file `./ssl/trusted.pem`. This file should contain only public keys of servers you trust. Therefore to trust our self-signed certificate we need to add its public key to our `trust.pem` file.

```PowerShell tab="Windows"
Get-Content -Path .\ssl\cert.pem | Add-Content .\ssl\trusted.pem
```

```bash tab="Linux"
cat ./ssl/cert.pem >> ./ssl/trusted.pem
```

## Configuring You Servers

Now we have everything set up to encrypt our communication with the Galaxia services. However, since we encrypted our private key with a password we need to give it to the applications serving a server in order to use it for encryption. Therefore you will need to append the parameter `--ssl-pkp <changeme>` to all applications running a server. If you are not sure whether they actually do you can always use the `--help` command.
