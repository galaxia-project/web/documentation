To configure an application, configurations can be passed using one of two options.

- *Environment*: Configurations are passed as environment variables.
- *Command Line*: Configurations are passed on application startup passed as flags following the application path.

This documentation will introduce you to both methods and how to apply them. 

!!! warning "Providing the same configuration using environments and command line."
    If a specific configuration is provided twice as the environment and command-line argument the implementation will always prefer the command line configuration.    

## Environment

The environment of the application scans on startup existing environment variables and applies them as default values to the application configuration. Therefore every configuration that is supported to be set using environment variables has corresponding environment name, see [reference](../reference).

Every environment configuration will search in the environment variables for two values. Once with the raw name (ie. `DATA_DIR`) and with an `XI` prefix applied (ie. `XI_DATA_DIR`). Note that the prefixed version is always preferred and if you provide both only the prefixed name will be considered. Additionally every application provides an application specific identifier to overwrite default environment variables (ie. `XI_DAEMON_DATA_DIR` or `DAEMON_DATA_DIR`).

One environment variable called `XI_ENV` is treated differently and has no prefixed version. If this environment variable is set every application will load all referenced [environment files](#files) on startup.

### Files

Environment files are used to persist configurations without explicitly stating them on every startup. An environment file contains a list of environment variables that will be overwritten in the application on startup.

!!! example "*.env*"
    ```bash
    # An exemplary environment file.

    DATA_DIR=~/galaxia/data
    WALLET_FILE=~/galaxia/wallet
    ```

Besides the standard variable assignment `<NAME>=<VALUE>` environment files support two extensions, comments, and nested environment variables. Comments are lines starting with `#` and are skipped by the application. Nested environment variables are references to other variables to retrieve the final value. Such nestings can be expressed using the syntax `${NAME}`. For example, the previous environment file could be rewritten to be easier to configure.

!!! example "*.env*"
    ```bash
    ROOT_DIR=~/galaxia

    DATA_DIR=${ROOT_DIR}/data
    WALLET_FILE=${ROOT_DIR}/wallet
    ```

Additionally to the predefined host environment variables the application will add additional variables before evaluation the environment files.

- *XI_APP* The name of the application.
- *XI_TIMESTAMP* The UNIX timestamp on application launch.
- *XI_VERSION* The full version of the application, ie. `1.7.3`
- *XI_VERSION_MAJOR* The current major version of the application.
- *XI_VERSION_MINOR* The current minor version of the application.
- *XI_VERSION_PATCH* The current patch version of the application.
- *XI_BUILD_CHANNEL* The default channel of the application, see [channels](../../infrastructure/channels)
- *XI_BUILD_BRANCH* The source branch in git that the current version was built form.
- *XI_BUILD_COMMIT* The source commit in git that the current version was built from.

Using the host environment and application predefined variables we can express much more complex configurations that can be changed more easily and are more applicable to other users host systems.

!!! example "An incomplete example demonstrating derived configurations."
    ```bash
    # User Configuration
    ROOT_DIR=/galaxia

    # Derived Configuration
    USER_ROOT_DIR=${ROOT_DIR}/${USER}

    ## Breakpad
    BREAKPAD=ON
    BREAKPAD_OUT=${USER_ROOT_DIR}/dump/${XI_APP}/v${XI_VERSION_MAJOR}_${XI_VERSION_MINOR}

    ## Log
    LOG_LEVEL=info
    LOF_FILE_LEVEL=trace
    LOG_FILE_PATH=${USER_ROOT_DIR}/log/${XI_APP}/${XI_TIMESTAMP}.log
    ```

### Chains

Environment files can be chained and are loaded consecutively in the order they are provided. Each consecutive environment file has access to previously defined environments and can override them.

To use this feature properly it is crucial to understand the order of evaluation.

1. The application sets predefined environments as previously mentioned.
2. If `XI_ENV` is set, every environment file is loaded in the order they appear. For example `XI_ENV=.env.dev,.env.light` would load `.env.dev` first.
3. If the current working directory contains a `.env` file it will be loaded.
4. If the current working directory contains a `.env.<app_name>` file it will be loaded (ie. `.env.wallet`).
5. Environment files passed as a command-line option are loaded, see [ENV command-line option](#env).

The given order induces the purpose of every method to provide environment files. The `XI_ENV` variable can setup global configurations a user wants to configure on every startup, for every chain. Using `.env` files users can set up a predefined configuration for all application using this working directory and can apply some application-specific configuration using the `.env.<app_name>` file. Lastly, the user can provide some overrides using the `ENV` command-line option to override some configurations on startup.

??? tip "Fork Setup"
    The previously mentioned structure can be used to create transparent forks without any interaction required by the user. Every fork will only need to provide the default network for the xi applications using the `.env` file.
    ```bash
    NETWORK=Nyan.MainNet
    ```

## Command Line

After every default configuration was loaded using the [environment system](#environment) command-line arguments are parsed. Those arguments are applied as strings following the launch command. For example, `./xi-wallet --data-dir ./test-sync` would override any previous data directory configuration with `./test-sync`. All command-line names must be prefixed with `--` except for shorthands. If a command-line parameter has a shorthand its name is written as `<shorthand> , <full>` in the [reference documentation](../reference).

### ENV

One special command-line argument is the `ENV` argument. This argument may not have any prefix and must exactly be written like `ENV=<files>...` where `<files>...` is a placeholder for multiple environment files loaded on startup (ie. `./xi-wallet ENV=light,wallet.miner`).